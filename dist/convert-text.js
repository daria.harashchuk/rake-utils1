"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.convertText = void 0;
const markdown_to_text_1 = __importDefault(require("markdown-to-text"));
const moment_1 = __importDefault(require("moment"));
const convertText = async ({ pack, cacheData }) => {
    const { message: { text = '', senderLabel = '', quotedMessage, type, }, } = pack;
    // This is enum, must be hosted on rake-utils
    const { messageType } = cacheData;
    // TODO check in DB if text is markdown
    // use next line of code only if text is markdown
    const normalizedText = markdown_to_text_1.default(text);
    if (!quotedMessage) {
        return type === messageType.endSession ? normalizedText : `${senderLabel}: ${normalizedText}`;
    }
    /**
     * Hard code of limitations for quotedMessage, must be put to DB
     */
    const LENGTH_LIMIT = 40; // 11 first ... 11 last
    let { sentAt, text: quotedMessageText, senderLabel: quotedMessageSender, } = quotedMessage;
    quotedMessageText = (quotedMessageText === null || quotedMessageText === void 0 ? void 0 : quotedMessageText.length) > LENGTH_LIMIT
        ? `"${(quotedMessageText || '').substr(0, LENGTH_LIMIT / 2)}...${(quotedMessageText || '')
            .substr(quotedMessageText.length - LENGTH_LIMIT / 2)}"`
        : quotedMessage.text || '';
    return `${senderLabel}:\nQuoted Message | "${quotedMessageText}" | ${quotedMessageSender} - ${moment_1.default(sentAt).format('ll')}\n---\n${text}`;
};
exports.convertText = convertText;
//# sourceMappingURL=convert-text.js.map
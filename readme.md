## Update Rake-Utils (var1)
The idea is that we translate Rake-Utils completely in TS, we use TS files for usage in TS projects.
But for JS projects we use compiled JS files.
So Rake-Utils should contain TS files and theirs duplicates in ./dist.

#### Usage
* after changes in code recompile js files
